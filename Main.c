#include<stdio.h>
#include<stdlib.h>

struct nodo {
    int info;
    struct nodo *izq;
    struct nodo *der;
};

struct nodo *raiz=NULL;

void insertar(int x)
{
    struct nodo *nuevo;
    nuevo = malloc(sizeof(struct nodo));
    nuevo->info = x;
    nuevo->izq = NULL;
    nuevo->der = NULL;
    if (raiz == NULL)
        raiz = nuevo;
    else
    {
        struct nodo *anterior, *reco;
        anterior = NULL;
        reco = raiz;
        while (reco != NULL)
        {
            anterior = reco;
            if (x < reco->info)
                reco = reco->izq;
            else
                reco = reco->der;
        }
        if (x < anterior->info)
            anterior->izq = nuevo;
        else
            anterior->der = nuevo;
    }
}

void imprimirPre(struct nodo *reco)
{
    if (reco != NULL)
    {
        printf("%i-",reco->info);
        imprimirPre(reco->izq);
        imprimirPre(reco->der);
    }
}


void imprimirIn(struct nodo *reco)
{
    if (reco != NULL)
    {
        imprimirIn(reco->izq);
        printf("%i-",reco->info);
        imprimirIn(reco->der);
    }
}

void imprimirPost(struct nodo *reco)
{
    if (reco != NULL)
    {
        imprimirPost(reco->izq);
        imprimirPost(reco->der);
        printf("%i-",reco->info);
    }
}

int main(){
  insertar(80);
  for(int i = 25; i <= 150; i = i + 25){
    insertar(i);
  }
  printf("Impresion pre-orden: ");
  imprimirPre(raiz);
  printf("\n\n");
  printf("Impresion in-orden: ");
  imprimirIn(raiz);
  printf("\n\n");
  printf("Impresion pos-orden: ");
  imprimirPost(raiz);
  return 0;
}